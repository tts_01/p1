import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { HomemapelPage } from './homemapel.page';

describe('HomemapelPage', () => {
  let component: HomemapelPage;
  let fixture: ComponentFixture<HomemapelPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HomemapelPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(HomemapelPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
