import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomemapelPage } from './homemapel.page';

const routes: Routes = [
  {
    path: '',
    component: HomemapelPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class HomemapelPageRoutingModule {}
